## Développement d'une ressource *pizzas*

### API et représentation des données


| URI                      | Opération   | MIME                                                         | Requête         | Réponse                                                       |
| :----------------------- | :---------- | :---------------------------------------------               | :--             | :----------------------------------------------------         |
| /pizzas            	   | GET         | <-application/json<br><-application/xml                      |                 | Liste des pizzas (P2)                                         |
| /pizzas/{id}       	   | GET         | <-application/json<br><-application/xml                      |                 | Une pizzas (P2) ou 404                                        |
| /pizzas/{id}/name   	   | GET         | <-text/plain                                                 |                 | Le nom de la pizzas ou 404                                    |
| /pizzas             	   | POST        | <-/->application/json<br>->application/x-www-form-urlencoded | Pizza (P1)      | Nouvelle pizza (P2)<br>409 si la pizza existe déjà (même nom) |
| /pizzas/{id}        	   | DELETE      | 												                |                 | Supprime une pizza                                            |   


Forme P1 : Lors de la création, la pizza n'est pas connu car elle sera fourni
		   par le JavaBean qui représente une pizza. Aussi on aura une
		   représentation JSON (P1) qui comporte uniquement le nom et la liste des ingrédients:

    {
      "name": "mozzarella"
      "ingredients" : ["id": "980c-149dcd81d306", "name": "Oignons"]
    }
    

Forme P2 : Une pizza comporte uniquement un identifiant, un nom et une liste d'ingrédient. Sa
		   représentation JSON (P2) prendra donc la forme suivante :

    {
      "id": "f38806a8-7c85-49ef-980c-149dcd81d306",
      "name": "mozzarella"
      "ingredients" : ["id": "980c-149dcd81d306", "name": "Oignons"]
    }



