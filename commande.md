## Développement d'une ressource *commandes*

### API et représentation des données


| URI                          | Opération   | MIME                                                         | Requête         | Réponse                                                       |
| :---------------------       | :---------- | :---------------------------------------------               | :--             | :----------------------------------------------------         |
| /commandes            	   | GET         | <-application/json<br><-application/xml                      |                 | Liste des commandes (P2)                                         |
| /commandes/{id}       	   | GET         | <-application/json<br><-application/xml                      |                 | Une commande (P2) ou 404                                        |
| /commandes/{id}/nom   	   | GET         | <-text/plain                                                 |                 | Le nom de celui qui passe la commande ou 404                                    |
| /commandes             	   | POST        | <-/->application/json<br>->application/x-www-form-urlencoded | Pizza (P1)      | Nouvelle commande (P2)<br>409 si la pizza existe déjà (même nom) |
| /commandes/{id}        	   | DELETE      | 											                    |                 | Supprime une commande                                            |   


Forme P1 : Lors de la création, la pizza n'est pas connu car elle sera fourni
		   par le JavaBean qui représente une pizza. Aussi on aura une
		   représentation JSON (P1) qui comporte uniquement le nom, prenom et la liste des pizzas :

    {
      "nom": "Thibaut"
      "prenom" : "Martin"
      "Pizzas" : [{"id": "980c-149dcd81d306", "name": "Reine", ["id": "980c-149dcd81d306", "name": "Oignons"]}, {"id": "980c-149dcd81d306", "name": "Espagnol", ["id": "980c-149dcd81d306", "name": "Oignons"]}]
    }
    

Forme P2 : Une commande comporte uniquement un identifiant, un nom, un prenom et une liste de pizza. Sa
		   représentation JSON (P2) prendra donc la forme suivante :

    {
      "id": "f38806a8-7c85-49ef-980c-149dcd81d306",
      "nom": "Thibaut"
      "prenom" : "Martin"
      "Pizzas" : [{"id": "980c-149dcd81d306", "name": "Reine", ["id": "980c-149dcd81d306", "name": "Oignons"]}, {"id": "980c-149dcd81d306", "name": "Espagnol", ["id": "980c-149dcd81d306", "name": "Oignons"]}]
    }



