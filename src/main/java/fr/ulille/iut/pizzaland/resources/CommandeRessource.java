package fr.ulille.iut.pizzaland.resources;

import java.net.URI;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Commande;
import fr.ulille.iut.pizzaland.dao.CommandeDao;
import fr.ulille.iut.pizzaland.dto.CommandeCreateDto;
import fr.ulille.iut.pizzaland.dto.CommandeDto;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;

@Produces("application/json")
@Path("/commandes")
public class CommandeRessource {
	private static final Logger LOGGER = Logger.getLogger(CommandeRessource.class.getName());

	private CommandeDao commandes;

	public CommandeRessource() {
		commandes = BDDFactory.buildDao(CommandeDao.class);
		commandes.createCommandeAndPizzaAssociation();
	}

	@Context
	public UriInfo uriInfo;

	@GET
	public List<CommandeDto> getAll() {
		LOGGER.info("CommandeRessource:getAll");

		List<CommandeDto> l = commandes.getAll().stream().map(Commande::toDto).collect(Collectors.toList());
		LOGGER.info(l.toString());
		return l;
	}
	
	@GET
	@Path("{id}")
	@Produces({ "application/json", "application/xml" })
	public CommandeDto getOneCommande(@PathParam("id") UUID id) {
		LOGGER.info("getOneCommande(" + id + ")");
		try {
			Commande commande = commandes.getTableAndPizzaAssociation(id);
			LOGGER.info(commande.toString());
			return Commande.toDto(commande);
		} catch (Exception e) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
	}
	
	@GET
	@Path("{id}/nom")
	@Produces({ "text/plain" })
	public String getCommandeName(@PathParam("id") UUID id) {
		Commande commande = commandes.findById(id);

		if (commande == null) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}

		return commande.getNom();
	}
	
	@POST
	@Consumes("application/x-www-form-urlencoded")
	public Response createCommande(CommandeCreateDto commandeCreateDto) {
		Commande existing = commandes.findByName(commandeCreateDto.getNom());
		if (existing != null) {
			throw new WebApplicationException(Response.Status.CONFLICT);
		}
		try {
			Commande c = Commande.fromCommandeCreateDto(commandeCreateDto);
			commandes.insertCommande(c);
			
			CommandeDto commandeDto = Commande.toDto(c);

			URI uri = uriInfo.getAbsolutePathBuilder().path(c.getId().toString()).build();

			return Response.created(uri).entity(commandeDto).build();
		} catch (Exception e) {
			e.printStackTrace();
			throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
		}
	}
	
	@DELETE
	@Path("{id}")
	public Response deleteCommande(@PathParam("id") UUID id) {
		if (commandes.findById(id) == null) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}

		commandes.remove(id);

		return Response.status(Response.Status.ACCEPTED).build();
	}
	
	
}
