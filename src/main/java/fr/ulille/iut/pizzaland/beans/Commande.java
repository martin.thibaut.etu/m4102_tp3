package fr.ulille.iut.pizzaland.beans;

import java.util.List;
import java.util.UUID;

import fr.ulille.iut.pizzaland.dto.CommandeCreateDto;
import fr.ulille.iut.pizzaland.dto.CommandeDto;

public class Commande {
	private String nom;
	private String prenom;
	private List<Pizza> pizzas;
	private UUID id = UUID.randomUUID();
	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public List<Pizza> getPizzas() {
		return pizzas;
	}
	public void setPizzas(List<Pizza> pizzas) {
		this.pizzas = pizzas;
	}
	public UUID getId() {
		return id;
	}
	public void setId(UUID id) {
		this.id = id;
	}
	
	public static CommandeDto toDto(Commande commande) {
		CommandeDto dto = new CommandeDto();
        dto.setId(commande.getId());
        dto.setNom(commande.getNom());
    	dto.setPrenom(commande.getPrenom());
        dto.setPizzas(commande.getPizzas());

        return dto;
    }
    
    public static Commande fromDto(CommandeDto dto) {
    	Commande commande = new Commande();
    	commande.setId(dto.getId());
    	commande.setNom(dto.getNom());
    	commande.setPrenom(dto.getPrenom());
    	commande.setPizzas(dto.getPizzas());
        return commande;
    }
    
    public static CommandeCreateDto toCreateDto(Commande commande) {
    	CommandeCreateDto dto = new CommandeCreateDto();
        dto.setNom(commande.getNom());
        dto.setPrenom(commande.getPrenom());
        return dto;
      }
    	
      public static Commande fromCommandeCreateDto(CommandeCreateDto createDto) {
    	  Commande commande = new Commande();
    	  commande.setNom(createDto.getNom());
    	  commande.setPrenom(createDto.getPrenom());
        return commande;
      }
      
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		result = prime * result + ((pizzas == null) ? 0 : pizzas.hashCode());
		result = prime * result + ((prenom == null) ? 0 : prenom.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Commande other = (Commande) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		if (pizzas == null) {
			if (other.pizzas != null)
				return false;
		} else if (!pizzas.equals(other.pizzas))
			return false;
		if (prenom == null) {
			if (other.prenom != null)
				return false;
		} else if (!prenom.equals(other.prenom))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Commande [nom=" + nom + ", prenom=" + prenom + ", pizzas=" + pizzas + ", id=" + id + "]";
	}
	
	
      
}
