package fr.ulille.iut.pizzaland.dto;

import java.util.UUID;

public class IngredientDto {

	private UUID id;
    private String name;

    public IngredientDto() {
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    public void setName(String name) {
      this.name = name;
    }

    public String getName() {
      return name;
    }
}
