package fr.ulille.iut.pizzaland.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


import fr.ulille.iut.pizzaland.beans.Ingredient;

public class PizzaDto {
	private String name;
	private List<Ingredient> ingredients = new ArrayList<>();
	private UUID id;
	
	public PizzaDto() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Ingredient> getIngredients() {
		return ingredients;
	}

	public void setIngredients(List<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}
	
	
}
