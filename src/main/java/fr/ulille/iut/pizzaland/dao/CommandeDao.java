package fr.ulille.iut.pizzaland.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Commande;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;

public interface CommandeDao {

	@SqlUpdate("CREATE TABLE IF NOT EXISTS Commande (id VARCHAR(128) PRIMARY KEY, nom VARCHAR UNIQUE NOT NULL, prenom VARCHAR UNIQUE NOT NULL)")
	void createCommandeTable();

	@SqlUpdate("CREATE TABLE IF NOT EXISTS CommandePizzaAssociation (commandeID VARCHAR(128), pizzaID VARCHAR(128), PRIMARY KEY(commandeID, pizzaID))")
	void createAssociationTable();

	@SqlUpdate("DROP TABLE IF EXISTS Commande")
	void dropTable();

	@SqlUpdate("DROP TABLE IF EXISTS CommandePizzaAssociation")
	void dropAssiociationTable();

	@SqlUpdate("INSERT INTO CommandePizzaAssociation (commandeID, pizzaID) VALUES (:c.id, :p.id)")
	void insertInAssociationTable(@BindBean("c") Commande commande, @BindBean("p") Pizza pizza);

	@SqlUpdate("INSERT INTO Commande (id, nom, prenom) VALUES (:id, :nom, :prenom)")
	void insertCommande(@BindBean Commande commande);

	@SqlQuery("SELECT * FROM Commande WHERE nom = :nom")
	@RegisterBeanMapper(Commande.class)
	Commande findByName(@Bind("nom") String nom);

	@SqlQuery("SELECT * FROM Commande")
	@RegisterBeanMapper(Commande.class)
	List<Commande> getAll();

	@SqlQuery("SELECT * FROM Commande WHERE id = :id")
	@RegisterBeanMapper(Commande.class)
	Commande findById(@Bind("id") UUID id);

	@SqlUpdate("DELETE FROM Commande WHERE id = :id")
	void remove(@Bind("id") UUID id);
	
	@Transaction
	default void createCommandeAndPizzaAssociation() {
		createAssociationTable();
		createCommandeTable();
	}

	@Transaction
	default void dropCommandeAndPizzaAssociation() {
		dropAssiociationTable();
		dropTable();
	}
	
	default void insertCommandeAndAssociation(Commande commande) {
		insertCommande(commande);
		for (Pizza pizza : commande.getPizzas()) {
			this.insertInAssociationTable(commande, pizza);
		}
	}
	
	@SqlQuery("SELECT pizzaID FROM CommandePizzaAssociation WHERE commandeID = :commandeID")
	List<UUID> getAllPizzaID(@Bind("commandeID") UUID commandeID);


	@Transaction
	default List<Pizza> getAllPizzas(List<UUID> pizzaID) {
		PizzaDao pizzas = BDDFactory.buildDao(PizzaDao.class);
		List<Pizza> pizza = new ArrayList<>();
		for (UUID id : pizzaID) {
			pizza.add(pizzas.findById(id));
		}
		return pizza;
	}

	@Transaction
	default Commande getTableAndPizzaAssociation(UUID id) {
		Commande commande = findById(id);
		List<UUID> pizzaID = getAllPizzaID(id);
		commande.setPizzas(getAllPizzas(pizzaID));
		return commande;
	}
}
