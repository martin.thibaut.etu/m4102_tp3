package fr.ulille.iut.pizzaland.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;

public interface PizzaDao {

	@SqlUpdate("CREATE TABLE IF NOT EXISTS Pizzas (id VARCHAR(128) PRIMARY KEY, name VARCHAR UNIQUE NOT NULL)")
	void createPizzaTable();

	@SqlUpdate("CREATE TABLE IF NOT EXISTS PizzaIngredientsAssociation (pizzaID VARCHAR(128), ingredientID VARCHAR(128), PRIMARY KEY(pizzaID, ingredientID))")
	void createAssociationTable();

	@SqlUpdate("DROP TABLE IF EXISTS Pizzas")
	void dropTable();

	@SqlUpdate("DROP TABLE IF EXISTS PizzaIngredientsAssociation")
	void dropAssiociationTable();

	@SqlUpdate("INSERT INTO PizzaIngredientsAssociation (pizzaID, ingredientID) VALUES (:p.id, :i.id)")
	void insertInAssociationTable(@BindBean("p") Pizza pizza, @BindBean("i") Ingredient ingredient);

	@SqlUpdate("INSERT INTO Pizzas (id, name) VALUES (:id, :name)")
	void insertPizza(@BindBean Pizza pizza);

	@SqlQuery("SELECT * FROM Pizzas WHERE name = :name")
	@RegisterBeanMapper(Pizza.class)
	Pizza findByName(@Bind("name") String name);

	@SqlQuery("SELECT * FROM Pizzas")
	@RegisterBeanMapper(Pizza.class)
	List<Pizza> getAll();

	@SqlQuery("SELECT * FROM Pizzas WHERE id = :id")
	@RegisterBeanMapper(Pizza.class)
	Pizza findById(@Bind("id") UUID id);

	@SqlUpdate("DELETE FROM Pizzas WHERE id = :id")
	void remove(@Bind("id") UUID id);

	@SqlQuery("SELECT ingredientID FROM PizzaIngredientsAssociation WHERE pizzaID = :idPizza")
	List<UUID> getAllIngredientsID(@Bind("idPizza") UUID idPizza);


	@Transaction
	default List<Ingredient> getAllIngredients(List<UUID> idIngredients) {
		IngredientDao ingredients = BDDFactory.buildDao(IngredientDao.class);
		List<Ingredient> ingredient = new ArrayList<>();
		for (UUID id : idIngredients) {
			ingredient.add(ingredients.findById(id));
		}
		return ingredient;
	}

	@Transaction
	default Pizza getTableAndIngredientAssociation(UUID id) {
		Pizza pizza = findById(id);
		List<UUID> ingredientsID = getAllIngredientsID(id);
		pizza.setIngredients(getAllIngredients(ingredientsID));
		return pizza;
	}

	@Transaction
	default void createTableAndIngredientAssociation() {
		createAssociationTable();
		createPizzaTable();
	}

	@Transaction
	default void dropTableAndIngredientAssociation() {
		dropAssiociationTable();
		dropTable();
	}

	default void insertPizzaAndAssociation(Pizza pizza) {
		insertPizza(pizza);
		for (Ingredient ingredient : pizza.getIngredients()) {
			this.insertInAssociationTable(pizza, ingredient);
		}
	}
}
