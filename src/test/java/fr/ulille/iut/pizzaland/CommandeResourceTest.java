package fr.ulille.iut.pizzaland;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.ulille.iut.pizzaland.beans.Commande;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.CommandeDao;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.CommandeCreateDto;
import fr.ulille.iut.pizzaland.dto.CommandeDto;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Application;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

public class CommandeResourceTest extends JerseyTest {
	private static final Logger LOGGER = Logger.getLogger(CommandeResourceTest.class.getName());
	private CommandeDao dao;
	private IngredientDao daoIng;
	private PizzaDao daoPizza;

	@Override
	protected Application configure() {
		BDDFactory.setJdbiForTests();
		return new ApiV1();
	}

	@Before
	public void setEnvUp() {
		dao = BDDFactory.buildDao(CommandeDao.class);
		dao.createCommandeAndPizzaAssociation();
		daoIng = BDDFactory.buildDao(IngredientDao.class);
		daoIng.createTable();
		daoPizza = BDDFactory.buildDao(PizzaDao.class);
		daoPizza.createTableAndIngredientAssociation();
	}

	@After
	public void tearEnvDown() throws Exception {
		dao.dropCommandeAndPizzaAssociation();
		daoIng.dropTable();
		daoPizza.dropTableAndIngredientAssociation();
	}
	
	@Test
	public void testGetEmptyList() {
		Response response = target("/commandes").request().get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
		List<CommandeDto> commandes;
		commandes = response.readEntity(new GenericType<List<CommandeDto>>() {
		});

		assertEquals(0, commandes.size());
	}
	
	@Test
	public void testGetCommandeList() {
		Commande commande = new Commande();
		commande.setNom("Thibaut");
		commande.setPrenom("Martin");
		List<Pizza> pizzaList = new ArrayList<>();
		List<Ingredient> ingredients = new ArrayList<>();
		Ingredient ingre = new Ingredient("Mozza");
		ingredients.add(ingre);
		daoIng.insert(ingre);
		
		Pizza p1 = new Pizza("Reine");
		p1.setIngredients(ingredients);
		
		pizzaList.add(p1);
		commande.setPizzas(pizzaList);
		
		daoPizza.insertPizzaAndAssociation(p1);
		dao.insertCommandeAndAssociation(commande);
		
		Response response = target("/commandes").request().get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
		
		List<CommandeDto> c;
		c = response.readEntity(new GenericType<List<CommandeDto>>() {
		});
		
		assertEquals(commande.getId(), Commande.fromDto(c.get(0)).getId());
		assertEquals(commande.getNom(), Commande.fromDto(c.get(0)).getNom());
		assertEquals(commande.getPrenom(), Commande.fromDto(c.get(0)).getPrenom());
	}
	
	@Test
	public void testGetExistingCommande() {

		Commande commande = new Commande();
		commande.setNom("Thibaut");
		commande.setPrenom("Martin");
		
		Pizza p = new Pizza("Reine");
		List<Pizza> pizzaList = new ArrayList<>();
		pizzaList.add(p);
		commande.setPizzas(pizzaList);
		
		daoPizza.insertPizza(p);
		dao.insertCommandeAndAssociation(commande);
		
		
		Response response = target("/commandes").path(commande.getId().toString()).request(MediaType.APPLICATION_JSON).get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		Commande result = Commande.fromDto(response.readEntity(CommandeDto.class));
		assertEquals(commande, result);
		assertEquals(commande.getPizzas().size(), result.getPizzas().size());
	}
	
	
	@Test
	public void testGetNotExistingCommande() {
		Response response = target("/commandes").path(UUID.randomUUID().toString()).request().get();
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}
	
	@Test
	public void testCreateCommande() {
		CommandeCreateDto commandeCreateDto = new CommandeCreateDto();
		commandeCreateDto.setNom("Thibaut");
		commandeCreateDto.setPrenom("Martin");
		
		Response response = target("/commandes").request().post(Entity.json(commandeCreateDto));

		assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());

		CommandeDto returnedEntity = response.readEntity(CommandeDto.class);

		
		try {
			assertEquals(target("/commandes/" + returnedEntity.getId()).getUri(), response.getLocation());
			assertEquals(commandeCreateDto.getPrenom(), returnedEntity.getPrenom());
			assertEquals(commandeCreateDto.getNom(), returnedEntity.getNom());
		} catch (Exception e) {
			new WebApplicationException(Response.Status.CONFLICT);
		}
	}
	
	@Test
	public void testCreateSameCommande() {
		Commande commande = new Commande();
		commande.setNom("Thibaut");
		commande.setPrenom("Martin");
		
		dao.insertCommande(commande);
		
		CommandeCreateDto commandeCreateDto = Commande.toCreateDto(commande);
		Response response = target("/commandes").request().post(Entity.json(commandeCreateDto));

		assertEquals(Response.Status.CONFLICT.getStatusCode(), response.getStatus());
	}
	
	@Test
	public void testCreateCommandeWithoutNom() {
		CommandeCreateDto commandeCreateDto = new CommandeCreateDto();

		Response response = target("/commandes").request().post(Entity.json(commandeCreateDto));

		assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
	}
	
	@Test
	public void testGetNomDeCeluiQuiFaitLaCommande() {
		Commande commande = new Commande();
		commande.setNom("Thibaut");
		commande.setPrenom("Martin");
		dao.insertCommande(commande);
		
		Response response = target("/commandes").path(commande.getId().toString()).path("nom").request().get();
		
		try {
			assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
			assertEquals("Thibaut", response.readEntity(String.class));
		} catch (Exception e) {
			new WebApplicationException(Response.Status.NOT_FOUND);
		}
		
		
	}
	
	@Test
	public void testDeleteExistingCommande() {
		Commande commande = new Commande();
		commande.setNom("Thibaut");
		commande.setPrenom("Martin");
		dao.insertCommande(commande);

		Response response = target("/commandes/").path(commande.getId().toString()).request().delete();

		assertEquals(Response.Status.ACCEPTED.getStatusCode(), response.getStatus());

		Commande result = dao.findById(commande.getId());
		assertEquals(result, null);
	}
	
	@Test
	public void testDeleteNotExistingCommande() {
		Response response = target("/commandes").path(UUID.randomUUID().toString()).request().delete();
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}

}
